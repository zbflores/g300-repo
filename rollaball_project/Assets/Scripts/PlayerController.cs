﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.InputSystem;
using TMPro;


public class PlayerController : MonoBehaviour
{
    //Public Variables
    public float speed = 0;
    public TextMeshProUGUI countText;
    public GameObject winTextObject;
    //Private Variables
    private Rigidbody rb;
    private int count;
    private float movementX;
    private float movementY;


    // Start is called before the first frame update
    void Start()
    {
        rb = GetComponent<Rigidbody>();
        count = 0;
        //Makes the 'Win Text' invisible on start.
        SetCountText();
        winTextObject.SetActive(false);

    }

    void OnMove(InputValue movementValue)
    {
        // Function body
        //Where movement input is processed.
        Vector2 movementVector = movementValue.Get<Vector2>();

        movementX = movementVector.x;
        movementY = movementVector.y;
    }

    void SetCountText()
    {
        //Where the counter displays and figures out if all the Pick Ups have been collected.
        countText.text = "Count: " + count.ToString();
        if(count >= 86)
        {
            winTextObject.SetActive(true);
        }
    }

    void FixedUpdate()
    {
        //Adds force to the player each frame.
        Vector3 movement = new Vector3(movementX, 0.0f, movementY);
        rb.AddForce(movement * speed);
    }

    private void OnTriggerEnter(Collider other)
    {  
        //Conditional statement to check PickUp tag.
        if (other.gameObject.CompareTag("PickUp"))
        {
            //Makes Pick Ups disappear when they are collected.
            other.gameObject.SetActive(false);
            count = count + 1;

            SetCountText();
            float scaleIncrease = .1f;
            //Variable to increase the speed based on each Pick Up collected.
            float speedscale = 0.025f;
            //Increases the transform scale for each Pick Up collected. (RollABall Script Modification 1)
            transform.localScale += new Vector3(scaleIncrease, scaleIncrease, scaleIncrease);
            //Where speed is added to the player's speed variable on a Pick Up collection. (RollABall Script Modification 2)
            speed = speed + speedscale;
            //Modification 3 (Non-Script) is the increasingly larger size of the level.
        }
    }
}
